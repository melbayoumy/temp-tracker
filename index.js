class TempTracker {
    _temps = [];
    _highest = null;
    _lowest = null;
    _sum = 0;

    insert(temp) {
        if ((!temp && temp !== 0) || typeof temp !== 'number') {
            return;
        }

        if (this._highest === null || temp > this._highest) {
            this._highest = temp;
        }

        if (this._lowest === null || temp < this._lowest) {
            this._lowest = temp;
        }

        this._temps.push(temp);
        this._sum += temp;

        console.log(`New record ${temp} ... ` +
                    `HIGHEST: ${this.getHighestTemp()} ` +
                    `- LOWEST: ${this.getLowestTemp()} ` +
                    `- AVERGAE: ${this.getAvgTemp()}`);
    }

    getHighestTemp() {
        return this._highest;
    }

    getLowestTemp() {
        return this._lowest;
    }

    getAvgTemp() {
        return Math.round((this._sum / this._temps.length) * 1000) / 1000; // round to 3 decimal places
    }
}

// Test cases
const testCases = new Map([
    ['Integers', [50, 90, 10, 40]],
    ['Decimals', [-65, -33.678, 13.555, 100.23, 54.876]],
    ['Empty', []],
    ['Wrong Data Types', [1, 's', false, '0', 5]],
    ['Zeros', [1, 0, '0', 5]],
]);

testCases.forEach((testCase, key) => {
    console.log("\n==============");
    console.log(`TEST CASE: ${key}`);
    console.log("==============\n");
    
    const tempTracker = new TempTracker();
    testCase.forEach((temp) => tempTracker.insert(temp));
});